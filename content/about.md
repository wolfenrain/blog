+++
title = "About"
date = "2020-07-08"
aliases = ["about-me"]
[ author ]
  name = "wolfenra.in"
+++

Hi there, I am `wolfenra.in` but you may call me Wolfen. I am a software developer from the Netherlands. 

This blog thing is a place where I write about the projects I am working on, or ideas that I have. Most of them are useless but oh boy are they fun. So grab a cup of tea and some biscuits and go see if there is something that pique your interest.
