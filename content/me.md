+++
title = "Jochum van der Ploeg (wolfenrain)"
date = "2020-09-10"
[ author ]
  name = "wolfenra.in"
+++

*Software developer based in Fryslân, Netherlands.*

[Email](mailto:jochum@vdploeg.net) / [Website](https://wolfenra.in) / [LinkedIn](https://www.linkedin.com/in/jochum-van-der-ploeg/) / [GitLab](https://gitlab.com/wolfenrain) / [GitHub](https://github.com/wolfenrain) / [Twitter](https://twitter.com/wolfenrain)

## Technical Experience

**Software Engineer** @ [Mobiléa](https://mobilea.nl) *(Mar 2018 - Present)*
> Startup company focused on video call solutions for the care sector.
> - Building custom made solutions that integrate with the Mobiléa platform.
> - Maintaining both the [`twilio_programmable-video`](https://gitlab.com/twilio-flutter/programmable-video) and [`twilio_programmable-chat`](https://gitlab.com/twilio-flutter/programmable-chat) Flutter plugins, both fully open-source.
> - ***Technologies used***: Flutter, NodeJS, TypeScript/JavaScript, Kotlin, Swift, Gitlab CI/CD. 

**Freelance webdevelopment** *(Sep 2016 - Mar 2018)*
> Worked on multiple website, either custom based or WordPress like systems. Most of the work was for [Kyzoe Hosting & Design](https://kyzoe.hosting).

## Opensource Experience

**Maintainer** @ [`twilio_programmable_chat`](https://gitlab.com/twilio-flutter/programmable-chat) *(May 2020 - Present)*
> A Flutter plugin for the [Twilio Programmable video](https://www.twilio.com/video?utm_source=opensource&utm_campaign=flutter-plugin) SDK.
> - Developing and maintaining the sourcode.
> - Handling bug and feature reports.
> - Reviewing merge requests from contributors.

**Maintainer** @ [`twilio_programmable_video`](https://gitlab.com/twilio-flutter/programmable-video) *(Jan 2020 - Present)*
> A Flutter plugin for the [Twilio Programmable Chat](https://www.twilio.com/chat?utm_source=opensource&utm_campaign=flutter-plugin) SDK.
> - Developing and maintaining the sourcode.
> - Handling bug and feature reports.
> - Reviewing merge requests from contributors.

**Contributor** @ [`flame`](https://flame-engine.org/) *(Dec 2019 - Present)*
> Contributing and working with the Flame Engine, Flame is a 2D game engine build on top of Flutter.

## Languages
**Dutch**: Native
**Frisian**: Native
**English**: B1

## Education
**Computer Science** for Bachelor's degree
> [Windesheim](https://www.windesheim.nl/) - Zwolle - Netherlands *(2018 - Present)*

**Computer software and media applications** 
> [Friesland College](https://www.frieslandcollege.nl/) - Heerenveen, Netherlands *(2015 - 2018)*
